var fromNumber = document.getElementById("from").value;
var toNumber = document.getElementById("to").value;
var mailNotification;

navigator.geolocation.getCurrentPosition(
    function (position) {
        alert('Your coords: ' +
            position.coords.latitude + ", " + position.coords.longitude);
    }
);

var notify = new Notification("permissed");
notify.onerror = function () {
    console.log("not permissed");
};

function getHiddenProp() {
    if ('hidden' in document) return 'hidden';
    return null;
}

function isHidden() {
    var prop = getHiddenProp();
    if (!prop) return false;

    return document[prop];
}

window.addEventListener("load", function notifyDemo() {
    var visProp = getHiddenProp();
    if (visProp) {
        var evtname = visProp.replace(/[H|h]idden/, '') + 'visibilitychange';
        document.addEventListener(evtname, visChange);
    }

    function visChange() {
        setTimeout(showNotification, 3000);
    }

    function showNotification() {
        if (isHidden()) {
            var mailNot = new Notification("Notification", {
                body: "EXIT"
            });
        }
    }
});

var statusDisplay = document.getElementById("status");
var searchButton = document.getElementById("searchButton");
var primeContainer = document.getElementById("primeContainer");
primeContainer.innerHTML = '';

function doSearch() {
    var fromNumber = Number(document.getElementById("from").value);
    var toNumber = Number(document.getElementById("to").value);
    localStorage.setItem("to", toNumber);

    if (localStorage.getItem("iterator") !== null) fromNumber = localStorage.getItem("iterator");
    if (localStorage.getItem("to") !== null) toNumber = localStorage.getItem("to");
    if (localStorage.getItem("mylist") !== null) primeContainer.innerHTML = '';

    searchButton.disabled = true;

    worker = new Worker("PrimeWorker.js");

    worker.onmessage = receivedWorkerMessage;
    worker.onerror = workerError;

    worker.postMessage(
        {
            from: fromNumber,
            to: toNumber
        }
    );

    statusDisplay.innerHTML = "Searching prime numbers (from " +
        fromNumber + " to " + toNumber + ") ...";
}

function workerError(error) {
    statusDisplay.innerHTML = error.message;
}

function cancelSearch() {
    worker.terminate();
    worker = null;
    statusDisplay.innerHTML = "Stopped";
    searchButton.disabled = false;
}


var worker = new Worker("PrimeWorker.js");

function receivedWorkerMessage(event) {
    var primes = event.data;
    localStorage.setItem("mylist", primes);

    var primeList = "";
    for (var i = 0; i < primes.length; i++) {
        primeList += primes[i];
        if (i !== primes.length - 1) primeList += ", ";
    }
    primeContainer.innerHTML += primeList;
    if (primeList.length === 0) {
        statusDisplay.innerHTML = "Error Search";
        searchButton.disabled = false;
        mailNotification = new Notification("Notification", {
            body: "Error Search"
        });
    }
    else {
        statusDisplay.innerHTML = "The prime numbers are:";
        searchButton.disabled = false;
        mailNotification = new Notification("Notification", {
            body: "The prime numbers are:"
        });
    }
}