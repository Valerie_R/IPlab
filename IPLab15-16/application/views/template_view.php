<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <title>ХАЦКЕРМЕН</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
    <link href="http://fonts.googleapis.com/css?family=Kreon" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <script src="/js/jquery-1.6.2.js" type="text/javascript"></script>
    <script type="text/javascript">
        // return a random integer between 0 and number
        function random(number) {

            return Math.floor(Math.random() * (number + 1));
        };

        // show random quote
        $(document).ready(function () {

            var quotes = $('.quote');
            quotes.hide();

            var qlen = quotes.length; //document.write( random(qlen-1) );
            $('.quote:eq(' + random(qlen - 1) + ')').show(); //tag:eq(1)
        });
    </script>
</head>
<body>
<div id="wrapper">
    <div id="header">
        <div id="logo">
            <a href="/">ХАЦКЕР</span> <span class="cms">МЕН</span></a>
        </div>
        <div id="menu">
            <ul>
                <li class="first active"><a href="/">Головненька</a></li>
                <li><a href="/services">Послуги</a></li>
                <li><a href="/portfolio">Портфоліо</a></li>
                <li class="last"><a href="/contacts">Контактики</a></li>
            </ul>
            <br class="clearfix"/>
        </div>
    </div>
    <div id="page">
        <div id="sidebar">
            <div class="side-box">
                <h3>Випадкові цитатт</h3>
                <p align="justify" class="quote">
                    «Сайт, как живой организм, изменяется и развивается.
                    Нельзя сразу написать идеальный вариант и на этом откланяться - это утопия»
                </p>
                <p align="justify" class="quote"><!-- &copy; Vitaly Swipe -->
                    «Все должно быть очень просто, как текстовый файл и при этом функционально
                    и тогда пользователи от нас уйдут»
                </p>
                </p>
            </div>
            <div class="side-box">
                <h3>Основне меню</h3>
                <ul class="list">
                    <li class="first "><a href="/">Головненька</a></li>
                    <li><a href="/services">Послуги</a></li>
                    <li><a href="/portfolio">Портфоліо</a></li>
                    <li class="last"><a href="/contacts">Контактики</a></li>
                </ul>
            </div>
        </div>
        <div id="content">
            <div class="box">
                <?php include 'application/views/' . $content_view; ?>
            </div>
            <br class="clearfix"/>
        </div>
        <br class="clearfix"/>
    </div>
    <div id="page-bottom">
        <div id="page-bottom-sidebar">
            <h3>Контакти</h3>
            <ul class="list">
                <li class="first">life: 0733253727</li>
                <li>київстар: 0676956017</li>
                <li class="last">email: yarik.loz12345@gmail.com</li>
            </ul>
        </div>
        <div id="page-bottom-content">
            <h3>Про наш кек</h3>
            <p>
                Вот дом.
                Который построил Джек.

                А это пшеница.
                Которая в тёмном чулане хранится
                В доме,
                Который построил Джек.

                А это весёлая птица-синица,
                Которая ловко ворует пшеницу,
                Которая в тёмном чулане хранится
                В доме,
                Который построил Джек.

                Вот кот,
                Который пугает и ловит синицу,
                Которая ловко ворует пшеницу,
                Которая в тёмном чулане хранится
                В доме,
                Который построил Джек.
            </p>
        </div>
        <br class="clearfix"/>
    </div>
</div>
<div id="footer">
    <a href="/">Хакермен</a> &copy; 2018</a>
</div>
</body>
</html>